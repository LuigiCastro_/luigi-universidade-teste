// using AutoMapper;
// using Microsoft.AspNetCore.Mvc;
// using Universidade.Domain.Entities;
// using Universidade.Domain.Contracts.Requests;
// using Universidade.Domain.Contracts.Response;
// using Universidade.Domain.Interfaces.Services;
// using Microsoft.AspNetCore.Authorization;
// using Universidade.Domain.Utils;

// namespace Universidade.Api.Controller;

// [Authorize(Roles = ConstanteUtil.PerfilProfessorNome)]
// public class ProfessorController : BaseController<ProfessorEntity, ProfessorRequest, ProfessorResponse>
// {
//     private readonly IMapper _mapper;
//     private readonly IProfessorService _professorService;
//     public ProfessorController(IMapper mapper, IProfessorService professorService) : base(mapper, professorService)
//     {
//         _mapper = mapper;
//         _professorService = professorService;
//     }


//     [HttpPatch("{id}")]
//     [ProducesResponseType(200)]
//     public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] ProfessorNomeRequest professorRequest)
//     {
//         await _professorService.AtualizarNomeAsync(id, professorRequest.Nome);
//         return Ok();
//     }


//     [HttpGet("[controller]{cpf}")]
//     [ProducesResponseType(200)]
//     public virtual async Task<ActionResult<ProfessorResponse>> GetByCpfAsync([FromRoute] string cpf)
//     {
//         var entity = await _professorService.ObterProfessorPorCpfAsync(cpf);
//         var reponse = _mapper.Map<ProfessorResponse>(entity);
//         return Ok(reponse);
//     }
// }