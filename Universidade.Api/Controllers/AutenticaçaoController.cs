using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Interfaces.Services;

namespace Universidade.Api.Controller;


[ApiController]
[Route("[controller]")]
[AllowAnonymous]
[ProducesResponseType(typeof(InformacaoResponse), 400)]
[ProducesResponseType(typeof(InformacaoResponse), 401)]
[ProducesResponseType(typeof(InformacaoResponse), 403)]
[ProducesResponseType(typeof(InformacaoResponse), 404)]
[ProducesResponseType(typeof(InformacaoResponse), 500)]
public class AutenticaçaoController : ControllerBase
{
    private readonly IUsuarioService _usuarioService;

    public AutenticaçaoController(IUsuarioService usuarioService)
    {   
        _usuarioService = usuarioService;
    }

    [HttpPost]
    [ProducesResponseType(200)]
    public async Task<ActionResult<AutenticaçaoResponse>> PostAsync([FromBody] AutenticaçaoRequest request)
    {
        var response = await _usuarioService.AutenticarUsuarioAsync(request.Email, request.Senha);
        return  Ok(response);
    }
}