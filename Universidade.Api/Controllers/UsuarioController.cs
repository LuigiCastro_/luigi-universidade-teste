using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Utils;

namespace Universidade.Api.Controller;


[Authorize(Roles = ConstanteUtil.PerfilLogadoNome)]
public class UsuarioController : BaseController<UsuarioEntity, UsuarioRequest, UsuarioResponse>
{
    private readonly IMapper _mapper;
    private readonly IUsuarioService _usuarioService;
    public UsuarioController(IMapper mapper, IUsuarioService usuarioService) : base(mapper, usuarioService)
    {
        _mapper = mapper;
        _usuarioService = usuarioService;
    }


    [HttpPost]
    [AllowAnonymous]
    [ProducesResponseType(201)]
    public override async Task<ActionResult> PostAsync([FromBody] UsuarioRequest usuarioRequest)
    {
        var entity = _mapper.Map<UsuarioEntity>(usuarioRequest);
        await _usuarioService.CriarUsuarioAsync(entity);
        return Created(nameof(PostAsync), new { id = entity.Id });
    }
    

    [HttpPut("{id}")]
    [ProducesResponseType(204)]
    public override async Task<ActionResult> PutAsync([FromRoute] int id, [FromBody] UsuarioRequest usuarioRequest)
    {
        var entity = _mapper.Map<UsuarioEntity>(usuarioRequest);
        entity.Id = id;
        await _usuarioService.AtualizarUsuarioAsync(entity);
        return NoContent();
    }


    [HttpPatch("{id}")]
    [ProducesResponseType(200)]
    public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] UsuarioTelefoneRequest usuarioRequest)
    {
        await _usuarioService.AtualizarTelefoneAsync(id, usuarioRequest.Telefone);
        return Ok();
    }


    [HttpGet("[controller]{nome}")]
    [ProducesResponseType(200)]
    public async Task<ActionResult<UsuarioResponse>> GetByNameAsync([FromQuery] string nome)
    {
        var entity = await _usuarioService.ObterAsync(x => x.Nome.Contains(nome));
        var response = _mapper.Map<UsuarioResponse>(entity);
        return Ok(response);
    }

    [HttpGet("{id}")]
    [ProducesResponseType(200)]
    public override async Task<ActionResult<UsuarioResponse>> GetByIdAsync([FromRoute] int id)
    {
        var entity = await _usuarioService.ObterPorIdAsync(id);
        return Ok(_mapper.Map<UsuarioResponse>(entity));
    }


    [HttpGet]
    [ProducesResponseType(200)]
    public override async Task<ActionResult<List<UsuarioResponse>>> GetAsync()
    {
        var entity = await _usuarioService.ObterTodosUsuarioAsync();
        return Ok(_mapper.Map<List<UsuarioResponse>>(entity));
        
    }


    
}