// using AutoMapper;
// using Microsoft.AspNetCore.Mvc;
// using Universidade.Domain.Entities;
// using Universidade.Domain.Contracts.Requests;
// using Universidade.Domain.Contracts.Response;
// using Universidade.Domain.Interfaces.Services;
// using Microsoft.AspNetCore.Authorization;
// using Universidade.Domain.Utils;

// namespace Universidade.Api.Controller;


// [Authorize(Roles = ConstanteUtil.PerfilLogadoNome)]
// public class AlunoController : BaseController<AlunoEntity, AlunoRequest, AlunoResponse>
// {
//     private readonly IMapper _mapper;
//     private readonly IAlunoService _alunoService;
//     public AlunoController(IMapper mapper, IAlunoService alunoService) : base(mapper, alunoService)
//     {
//         _mapper = mapper;
//         _alunoService = alunoService;
//     }


//     [HttpPatch("{id}")]
//     [AllowAnonymous]
//     [ProducesResponseType(200)]
//     public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] AlunoMatriculaRequest alunoRequest)
//     {
//         await _alunoService.AtualizarMatriculaAsync(id, alunoRequest.Matricula);
//         return Ok();
//     }

//     [HttpGet("[controller]{matricula}")]
//     [ProducesResponseType(200)]
//     public virtual async Task<ActionResult<AlunoResponse>> GetByMatriculaAsync([FromRoute] string matricula)
//     {
//         var entity = await _alunoService.ObterAlunoPorMatriculaAsync(matricula);
//         var reponse = _mapper.Map<AlunoResponse>(entity);
//         return Ok(reponse);
//     }
// }