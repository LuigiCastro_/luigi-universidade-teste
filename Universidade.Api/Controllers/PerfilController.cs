using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Universidade.Domain.Entities;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;

namespace Universidade.Api.Controller;


public class PerfilController : BaseController<PerfilEntity, PerfilRequest, PerfilResponse>
{
    private readonly IMapper _mapper;
    private readonly IPerfilService _perfilService;
    public PerfilController(IMapper mapper, IPerfilService perfilService) : base(mapper, perfilService)
    {
        _mapper = mapper;
        _perfilService = perfilService;
    }


    [HttpPost]
    [AllowAnonymous]
    [ProducesResponseType(201)]
    public override async Task<ActionResult> PostAsync([FromBody] PerfilRequest request)
    {
        var entity = _mapper.Map<PerfilEntity>(request);
        await _perfilService.AdicionarAsync(entity);
        return Created(nameof(PostAsync), new { id = entity.Id });
    }
}