using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Utils;

namespace Universidade.Api.Controller;


[ApiController]
[Route("[controller]")]
public class BaseController<Entity, Request, Response> : ControllerBase where Entity : BaseEntity
{
    private readonly IMapper _mapper;
    private IBaseService<Entity> _service;
    
    public BaseController(IMapper mapper, IBaseService<Entity> service)
    {
        _mapper = mapper;
        _service = service;
    }


   [HttpPost]
   [AllowAnonymous]
   [ProducesResponseType(201)]
   public virtual async Task<ActionResult> PostAsync([FromBody] Request request)
   {
        var entity = _mapper.Map<Entity>(request);
        await _service.AdicionarAsync(entity);
        return Created(nameof(PostAsync), new { id = entity.Id } );
   }
   

   [HttpPut("{id}")]
   [ProducesResponseType(204)]
   public virtual async Task<ActionResult> PutAsync([FromRoute] int id, [FromBody] Request request)
   {
        var entity = _mapper.Map<Entity>(request);
        entity.Id = id;
        await _service.AlterarAsync(entity);
        return NoContent();
   }
   

   [HttpGet]
   [ProducesResponseType(200)]
   public virtual async Task<ActionResult<List<Response>>> GetAsync()
   {
        var entitiesList = await _service.ObterTodosAsync();
        var responseList = _mapper.Map<List<Response>>(entitiesList);
        return Ok(responseList);
   }
   

   [HttpGet("{id}")]
   [ProducesResponseType(200)]
   public virtual async Task<ActionResult<Response>> GetByIdAsync([FromRoute] int id)
   {
        var entity = await _service.ObterPorIdAsync(id);
        var reponse = _mapper.Map<Response>(entity);
        return Ok(reponse);
   }

   
   [HttpDelete("{id}")]
   [ProducesResponseType(204)]
   public virtual async Task<ActionResult> DeleteAsync([FromRoute] int id)
   {
        await _service.DeletarAsync(id);
        return NoContent();
   }

    

}