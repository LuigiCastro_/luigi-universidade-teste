using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Interfaces.Service;
using Universidade.Domain.Utils;

namespace Universidade.Api.Controller;

[ApiController]
[Route("[controller]")]
[Authorize(Roles = ConstanteUtil.PerfilProfessorNome)]
public class LocalController : ControllerBase
{
    private readonly ILocalService _localService;

    public LocalController(ILocalService localService)
    {
        _localService = localService;
    }


    [HttpGet]
    [Route("cep")]
    public async Task<LocalResponse> ObterDadosPorCep(string cep)
    {
        return await _localService.ObterDadosPorCep(cep);
    }


    [HttpGet]
    [Route("municipio")]
    public async Task<object> ObterDadosPorMunicipio()
    {
        return await _localService.ObterDadosPorMunicipio();
    }


    [HttpGet]
    [Route("estado")]
    public async Task<object> ObterDadosPorEstado()
    {
        return await _localService.ObterDadosPorEstado();
    }
}
