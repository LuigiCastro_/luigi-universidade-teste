using AutoMapper;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;

namespace Universidade.Api.Profiles;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        # region Request to Entity
        
        // CreateMap<AlunoRequest, AlunoEntity>().ReverseMap();
        CreateMap<CursoRequest, CursoEntity>().ReverseMap();
        CreateMap<DepartamentoRequest, DepartamentoEntity>().ReverseMap();
        CreateMap<EndereçoRequest, EndereçoEntity>().ReverseMap();
        // CreateMap<ProfessorRequest, ProfessorEntity>().ReverseMap();
        CreateMap<UsuarioRequest, UsuarioEntity>().ReverseMap();
        CreateMap<PerfilRequest, PerfilEntity>().ReverseMap();

        # endregion

        # region Entity to Response
        
        // CreateMap<AlunoEntity, AlunoResponse>().ReverseMap();
        CreateMap<CursoEntity, CursoResponse>().ReverseMap();
        CreateMap<DepartamentoEntity, DepartamentoResponse>().ReverseMap();
        CreateMap<EndereçoEntity, EndereçoResponse>().ReverseMap();
        // CreateMap<ProfessorEntity, ProfessorResponse>().ReverseMap();
        CreateMap<UsuarioEntity, UsuarioResponse>().ReverseMap();
        CreateMap<PerfilEntity, PerfilResponse>().ReverseMap();

        # endregion
    }
}