using Universidade.Infra.Context;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;

namespace Universidade.Infra.Repositories;

public class UsuarioRepository : BaseRepository<UsuarioEntity>, IUsuarioRepository
{
    public UsuarioRepository(ManutençaoContext context) : base(context)
    {
        
    }
    
}