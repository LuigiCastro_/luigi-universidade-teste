using Universidade.Infra.Context;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;

namespace Universidade.Infra.Repositories;

public class PerfilRepository : BaseRepository<PerfilEntity>, IPerfilRepository
{
    public PerfilRepository(ManutençaoContext context) : base(context)
    {
        
    }
    
}