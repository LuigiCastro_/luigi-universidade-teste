using Universidade.Infra.Context;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;

namespace Universidade.Infra.Repositories;

public class CursoRepository : BaseRepository<CursoEntity>, ICursoRepository
{
    public CursoRepository(ManutençaoContext context) : base(context)
    {
        
    }
    
}