using Microsoft.EntityFrameworkCore;
using Universidade.Domain.Entities;

namespace Universidade.Infra.Context;

public class ManutençaoContext : DbContext
{
    public ManutençaoContext() { }
    public ManutençaoContext(DbContextOptions<ManutençaoContext> options) : base(options)
    {
    }

    // public DbSet<AlunoEntity> Alunos {get; set;}
    public DbSet<CursoEntity> Cursos {get; set;}
    public DbSet<DepartamentoEntity> Departamentos {get; set;}
    public DbSet<EndereçoEntity> Endereços {get; set;}
    // public DbSet<ProfessorEntity> Professores {get; set;}
    public DbSet<UsuarioEntity> Usuarios {get; set;}
    public DbSet<PerfilEntity> Perfis {get; set;}

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
       optionsBuilder.UseSqlServer(@"Server=44.199.200.211,3309;Database=universidade_luigi_castro;User=luigi_castro;Password=PcRgWM8LZLi3haE3BBWu");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // modelBuilder.Entity<AlunoEntity>().HasOne(x => x.Endereço).WithMany(x => x.Alunos).OnDelete(DeleteBehavior.Restrict);
        // modelBuilder.Entity<AlunoEntity>().HasOne(x => x.Curso).WithMany(x => x.Alunos).OnDelete(DeleteBehavior.Restrict);
        
        modelBuilder.Entity<CursoEntity>().HasOne(x => x.Departamento).WithMany(x => x.Cursos).OnDelete(DeleteBehavior.Restrict);
        
        modelBuilder.Entity<DepartamentoEntity>().HasOne(x => x.Endereço).WithMany(x => x.Departamentos).OnDelete(DeleteBehavior.Restrict);
        
        // modelBuilder.Entity<ProfessorEntity>().HasOne(x => x.Endereço).WithMany(x => x.Professores).OnDelete(DeleteBehavior.Restrict);
        // modelBuilder.Entity<ProfessorEntity>().HasOne(x => x.Departamento).WithMany(x => x.Professores).OnDelete(DeleteBehavior.Restrict);
        
        modelBuilder.Entity<UsuarioEntity>().HasOne(x => x.Perfil).WithMany(x => x.Usuarios).OnDelete(DeleteBehavior.Restrict);
        modelBuilder.Entity<UsuarioEntity>().HasOne(x => x.Endereço).WithMany(x => x.Usuarios).OnDelete(DeleteBehavior.Restrict);
        modelBuilder.Entity<UsuarioEntity>().HasOne(x => x.Departamento).WithMany(x => x.Usuarios).OnDelete(DeleteBehavior.Restrict);
        modelBuilder.Entity<UsuarioEntity>().HasOne(x => x.Curso).WithMany(x => x.Usuarios).OnDelete(DeleteBehavior.Restrict);
    }
}