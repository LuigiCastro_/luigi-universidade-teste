using System.Net.Http;

namespace Universidade.Teste.Fakers
{
    public class HttpResponseMessageFaker
    {
        public HttpContent Content { get; set; }

        public bool IsSuccessStatusCode { get; set; }
    }
}
