using Universidade.Domain.Contracts.Response;
using Bogus;

namespace Universidade.Teste.Fakers
{
    public static class LocalResponseFaker
    {
        private static readonly Faker Fake = new Faker();

        public static LocalResponse Local() => new LocalResponse
        {
            Cep = Fake.Address.ZipCode(),
            Logradouro = Fake.Address.StreetName(),
            Complemento = Fake.Address.Country(),
            Bairro = Fake.Address.FullAddress(),
            Localidade = Fake.Address.StreetSuffix(),
            Uf = Fake.Address.CitySuffix()
        };
    }
}
