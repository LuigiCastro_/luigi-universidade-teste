using Bogus;
using Universidade.Domain.Contracts.Http;

namespace Universidade.Teste.Fakers
{
    public static class HttpOptionsFaker
    {
        private static readonly Faker Fake = new Faker();

        public static HttpCepOptions CepOptions() => new HttpCepOptions
        {
            Url = Fake.Internet.Url(),
            AposQueryString = Fake.Name.FirstName()
        };
    }
}
