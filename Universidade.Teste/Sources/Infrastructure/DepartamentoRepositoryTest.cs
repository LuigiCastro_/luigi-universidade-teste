using Universidade.Infra.Repositories;
using Microsoft.EntityFrameworkCore;
using Universidade.Domain.Entities;
using Universidade.Teste.Configs;
using Universidade.Infra.Context;
using Moq.EntityFrameworkCore;
using AutoFixture;
using Xunit;
using Moq;

namespace Universidade.Teste.Sources.Infrastructure.Repositories
{
    [Trait("Repository", "Repository Departamento")]
    public class DepartamentoRepositoryTest
    {
        private readonly Mock<ManutençaoContext> _mockContext;
        private readonly Fixture _fixture;

        public DepartamentoRepositoryTest()
        {
            _mockContext = new Mock<ManutençaoContext>(new DbContextOptionsBuilder<ManutençaoContext>().UseLazyLoadingProxies().Options);
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Cadastra Departamento")]
        public async Task Post()
        {
            var entity = _fixture.Create<DepartamentoEntity>();
            
            _mockContext.Setup(mock => mock.Set<DepartamentoEntity>()).ReturnsDbSet(new List<DepartamentoEntity> { new DepartamentoEntity() });
            var repository = new DepartamentoRepository(_mockContext.Object);

            try
            {
                await repository.AddAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Fact(DisplayName = "Cadastra Departamento")]
        public async Task PostNullException()
        {
            var entity = _fixture.Create<DepartamentoEntity>();

            _mockContext.Setup(mock => mock.Set<DepartamentoEntity>()).ReturnsDbSet(new List<DepartamentoEntity>());

            var repository = new DepartamentoRepository(_mockContext.Object);
            var exception = await Record.ExceptionAsync(() => repository.AddAsync(entity));
            
            Assert.Null(exception);
        }


        [Fact(DisplayName = "Cadastra Departamento")]
        public async Task PostDepartamentoNotNull()
        {
            var entity = _fixture.Create<DepartamentoEntity>();

            _mockContext.Setup(mock => mock.Set<DepartamentoEntity>()).ReturnsDbSet(new List<DepartamentoEntity>());

            var repository = new DepartamentoRepository(_mockContext.Object);
            Assert.NotNull(repository.AddAsync(entity));
        }


        [Fact(DisplayName = "Edita Departamento Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<DepartamentoEntity>();

            _mockContext.Setup(mock => mock.Set<DepartamentoEntity>()).ReturnsDbSet(new List<DepartamentoEntity> { new DepartamentoEntity() });
            var repository = new DepartamentoRepository(_mockContext.Object);

            try
            {
                await repository.EditAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Fact(DisplayName = "Lista Departamento")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<DepartamentoEntity>>();

            _mockContext.Setup(mock => mock.Set<DepartamentoEntity>()).ReturnsDbSet(entities);

            var repository = new DepartamentoRepository(_mockContext.Object);
            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Departamento Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<DepartamentoEntity>();

            _mockContext.Setup(mock => mock.Set<DepartamentoEntity>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var repository = new DepartamentoRepository(_mockContext.Object);
            var response = await repository.FindAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Departamento Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<DepartamentoEntity>();

            _mockContext.Setup(mock => mock.Set<DepartamentoEntity>()).ReturnsDbSet(new List<DepartamentoEntity> { entity });
            var repository = new DepartamentoRepository(_mockContext.Object);

            try
            {
                await repository.RemoveAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
