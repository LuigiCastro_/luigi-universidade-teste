using Universidade.Infra.Repositories;
using Microsoft.EntityFrameworkCore;
using Universidade.Domain.Entities;
using Universidade.Teste.Configs;
using Universidade.Infra.Context;
using Moq.EntityFrameworkCore;
using AutoFixture;
using Xunit;
using Moq;

namespace Universidade.Teste.Sources.Infrastructure.Repositories
{
    [Trait("Repository", "Repository Endereço")]
    public class EndereçoRepositoryTest
    {
        private readonly Mock<ManutençaoContext> _mockContext;
        private readonly Fixture _fixture;

        public EndereçoRepositoryTest()
        {
            _mockContext = new Mock<ManutençaoContext>(new DbContextOptionsBuilder<ManutençaoContext>().UseLazyLoadingProxies().Options);
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Cadastra Endereço")]
        public async Task Post()
        {
            var entity = _fixture.Create<EndereçoEntity>();
            
            _mockContext.Setup(mock => mock.Set<EndereçoEntity>()).ReturnsDbSet(new List<EndereçoEntity> { new EndereçoEntity() });
            var repository = new EndereçoRepository(_mockContext.Object);

            try
            {
                await repository.AddAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Fact(DisplayName = "Cadastra Endereço")]
        public async Task PostNullException()
        {
            var entity = _fixture.Create<EndereçoEntity>();

            _mockContext.Setup(mock => mock.Set<EndereçoEntity>()).ReturnsDbSet(new List<EndereçoEntity>());

            var repository = new EndereçoRepository(_mockContext.Object);
            var exception = await Record.ExceptionAsync(() => repository.AddAsync(entity));
            
            Assert.Null(exception);
        }


        [Fact(DisplayName = "Cadastra Endereço")]
        public async Task PostEndereçoNotNull()
        {
            var entity = _fixture.Create<EndereçoEntity>();

            _mockContext.Setup(mock => mock.Set<EndereçoEntity>()).ReturnsDbSet(new List<EndereçoEntity>());

            var repository = new EndereçoRepository(_mockContext.Object);
            Assert.NotNull(repository.AddAsync(entity));
        }


        [Fact(DisplayName = "Edita Endereço Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<EndereçoEntity>();

            _mockContext.Setup(mock => mock.Set<EndereçoEntity>()).ReturnsDbSet(new List<EndereçoEntity> { new EndereçoEntity() });
            var repository = new EndereçoRepository(_mockContext.Object);

            try
            {
                await repository.EditAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Fact(DisplayName = "Lista Endereço")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<EndereçoEntity>>();

            _mockContext.Setup(mock => mock.Set<EndereçoEntity>()).ReturnsDbSet(entities);

            var repository = new EndereçoRepository(_mockContext.Object);
            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Endereço Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<EndereçoEntity>();

            _mockContext.Setup(mock => mock.Set<EndereçoEntity>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var repository = new EndereçoRepository(_mockContext.Object);
            var response = await repository.FindAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Endereço Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<EndereçoEntity>();

            _mockContext.Setup(mock => mock.Set<EndereçoEntity>()).ReturnsDbSet(new List<EndereçoEntity> { entity });
            var repository = new EndereçoRepository(_mockContext.Object);

            try
            {
                await repository.RemoveAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
