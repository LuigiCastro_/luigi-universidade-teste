using Universidade.Infra.Repositories;
using Microsoft.EntityFrameworkCore;
using Universidade.Domain.Entities;
using Universidade.Teste.Configs;
using Universidade.Infra.Context;
using Moq.EntityFrameworkCore;
using AutoFixture;
using Xunit;
using Moq;

namespace Universidade.Teste.Sources.Infrastructure.Repositories
{
    [Trait("Repository", "Repository Perfil")]
    public class PerfilRepositoryTest
    {
        private readonly Mock<ManutençaoContext> _mockContext;
        private readonly Fixture _fixture;

        public PerfilRepositoryTest()
        {
            _mockContext = new Mock<ManutençaoContext>(new DbContextOptionsBuilder<ManutençaoContext>().UseLazyLoadingProxies().Options);
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Cadastra Perfil")]
        public async Task Post()
        {
            var entity = _fixture.Create<PerfilEntity>();
            
            _mockContext.Setup(mock => mock.Set<PerfilEntity>()).ReturnsDbSet(new List<PerfilEntity> { new PerfilEntity() });
            var repository = new PerfilRepository(_mockContext.Object);

            try
            {
                await repository.AddAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Fact(DisplayName = "Cadastra Perfil")]
        public async Task PostNullException()
        {
            var entity = _fixture.Create<PerfilEntity>();

            _mockContext.Setup(mock => mock.Set<PerfilEntity>()).ReturnsDbSet(new List<PerfilEntity>());

            var repository = new PerfilRepository(_mockContext.Object);
            var exception = await Record.ExceptionAsync(() => repository.AddAsync(entity));
            
            Assert.Null(exception);
        }


        [Fact(DisplayName = "Cadastra Perfil")]
        public async Task PostPerfilNotNull()
        {
            var entity = _fixture.Create<PerfilEntity>();

            _mockContext.Setup(mock => mock.Set<PerfilEntity>()).ReturnsDbSet(new List<PerfilEntity>());

            var repository = new PerfilRepository(_mockContext.Object);
            Assert.NotNull(repository.AddAsync(entity));
        }


        [Fact(DisplayName = "Edita Perfil Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<PerfilEntity>();

            _mockContext.Setup(mock => mock.Set<PerfilEntity>()).ReturnsDbSet(new List<PerfilEntity> { new PerfilEntity() });
            var repository = new PerfilRepository(_mockContext.Object);

            try
            {
                await repository.EditAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Fact(DisplayName = "Lista Perfil")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<PerfilEntity>>();

            _mockContext.Setup(mock => mock.Set<PerfilEntity>()).ReturnsDbSet(entities);

            var repository = new PerfilRepository(_mockContext.Object);
            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Perfil Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<PerfilEntity>();

            _mockContext.Setup(mock => mock.Set<PerfilEntity>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var repository = new PerfilRepository(_mockContext.Object);
            var response = await repository.FindAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Perfil Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<PerfilEntity>();

            _mockContext.Setup(mock => mock.Set<PerfilEntity>()).ReturnsDbSet(new List<PerfilEntity> { entity });
            var repository = new PerfilRepository(_mockContext.Object);

            try
            {
                await repository.RemoveAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
