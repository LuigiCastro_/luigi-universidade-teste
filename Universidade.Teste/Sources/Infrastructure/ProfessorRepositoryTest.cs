// using Universidade.Infra.Repositories;
// using Microsoft.EntityFrameworkCore;
// using Universidade.Domain.Entities;
// using Universidade.Teste.Configs;
// using Universidade.Infra.Context;
// using Moq.EntityFrameworkCore;
// using AutoFixture;
// using Xunit;
// using Moq;

// namespace Universidade.Teste.Sources.Infrastructure.Repositories
// {
//     [Trait("Repository", "Repository Professor")]
//     public class ProfessorRepositoryTest
//     {
//         private readonly Mock<ManutençaoContext> _mockContext;
//         private readonly Fixture _fixture;

//         public ProfessorRepositoryTest()
//         {
//             _mockContext = new Mock<ManutençaoContext>(new DbContextOptionsBuilder<ManutençaoContext>().UseLazyLoadingProxies().Options);
//             _fixture = FixtureConfig.Get();
//         }


//         [Fact(DisplayName = "Cadastra Professor")]
//         public async Task Post()
//         {
//             var entity = _fixture.Create<ProfessorEntity>();
            
//             _mockContext.Setup(mock => mock.Set<ProfessorEntity>()).ReturnsDbSet(new List<ProfessorEntity> { new ProfessorEntity() });
//             var repository = new ProfessorRepository(_mockContext.Object);

//             try
//             {
//                 await repository.AddAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }


//         [Fact(DisplayName = "Edita Professor Existente")]
//         public async Task Put()
//         {
//             var entity = _fixture.Create<ProfessorEntity>();

//             _mockContext.Setup(mock => mock.Set<ProfessorEntity>()).ReturnsDbSet(new List<ProfessorEntity> { new ProfessorEntity() });
//             var repository = new ProfessorRepository(_mockContext.Object);

//             try
//             {
//                 await repository.EditAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }


//         [Fact(DisplayName = "Lista Professores")]
//         public async Task Get()
//         {
//             var entities = _fixture.Create<List<ProfessorEntity>>();

//             _mockContext.Setup(mock => mock.Set<ProfessorEntity>()).ReturnsDbSet(entities);

//             var repository = new ProfessorRepository(_mockContext.Object);
//             var response = await repository.ListAsync();

//             Assert.True(response.Count() > 0);
//         }


//         [Fact(DisplayName = "Busca Professor Id")]
//         public async Task GetById()
//         {
//             var entity = _fixture.Create<ProfessorEntity>();

//             _mockContext.Setup(mock => mock.Set<ProfessorEntity>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

//             var repository = new ProfessorRepository(_mockContext.Object);
//             var response = await repository.FindAsync(entity.Id);

//             Assert.Equal(response.Id, entity.Id);
//         }


//         [Fact(DisplayName = "Remove Professor Existente")]
//         public async Task Delete()
//         {
//             var entity = _fixture.Create<ProfessorEntity>();

//             _mockContext.Setup(mock => mock.Set<ProfessorEntity>()).ReturnsDbSet(new List<ProfessorEntity> { entity });
//             var repository = new ProfessorRepository(_mockContext.Object);

//             try
//             {
//                 await repository.RemoveAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }
//     }
// }
