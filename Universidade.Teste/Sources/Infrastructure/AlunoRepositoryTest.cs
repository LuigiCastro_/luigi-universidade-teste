// using Universidade.Infra.Repositories;
// using Microsoft.EntityFrameworkCore;
// using Universidade.Domain.Entities;
// using Universidade.Teste.Configs;
// using Universidade.Infra.Context;
// using Moq.EntityFrameworkCore;
// using AutoFixture;
// using Xunit;
// using Moq;

// namespace Universidade.Teste.Sources.Infrastructure.Repositories
// {
//     [Trait("Repository", "Repository Aluno")]
//     public class AlunoRepositoryTest
//     {
//         private readonly Mock<ManutençaoContext> _mockContext;
//         private readonly Fixture _fixture;

//         public AlunoRepositoryTest()
//         {
//             _mockContext = new Mock<ManutençaoContext>(new DbContextOptionsBuilder<ManutençaoContext>().UseLazyLoadingProxies().Options);
//             _fixture = FixtureConfig.Get();
//         }


//         [Fact(DisplayName = "Cadastra Aluno")]
//         public async Task Post()
//         {
//             var entity = _fixture.Create<AlunoEntity>();
            
//             _mockContext.Setup(mock => mock.Set<AlunoEntity>()).ReturnsDbSet(new List<AlunoEntity> { new AlunoEntity() });
//             var repository = new AlunoRepository(_mockContext.Object);

//             try
//             {
//                 await repository.AddAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }


//         [Fact(DisplayName = "Edita Aluno Existente")]
//         public async Task Put()
//         {
//             var entity = _fixture.Create<AlunoEntity>();

//             _mockContext.Setup(mock => mock.Set<AlunoEntity>()).ReturnsDbSet(new List<AlunoEntity> { new AlunoEntity() });
//             var repository = new AlunoRepository(_mockContext.Object);

//             try
//             {
//                 await repository.EditAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }


//         [Fact(DisplayName = "Lista Aluno")]
//         public async Task Get()
//         {
//             var entities = _fixture.Create<List<AlunoEntity>>();

//             _mockContext.Setup(mock => mock.Set<AlunoEntity>()).ReturnsDbSet(entities);

//             var repository = new AlunoRepository(_mockContext.Object);
//             var response = await repository.ListAsync();

//             Assert.True(response.Count() > 0);
//         }


//         [Fact(DisplayName = "Busca Aluno Id")]
//         public async Task GetById()
//         {
//             var entity = _fixture.Create<AlunoEntity>();

//             _mockContext.Setup(mock => mock.Set<AlunoEntity>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

//             var repository = new AlunoRepository(_mockContext.Object);
//             var response = await repository.FindAsync(entity.Id);

//             Assert.Equal(response.Id, entity.Id);
//         }


//         [Fact(DisplayName = "Remove Aluno Existente")]
//         public async Task Delete()
//         {
//             var entity = _fixture.Create<AlunoEntity>();

//             _mockContext.Setup(mock => mock.Set<AlunoEntity>()).ReturnsDbSet(new List<AlunoEntity> { entity });
//             var repository = new AlunoRepository(_mockContext.Object);

//             try
//             {
//                 await repository.RemoveAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }
//     }
// }
