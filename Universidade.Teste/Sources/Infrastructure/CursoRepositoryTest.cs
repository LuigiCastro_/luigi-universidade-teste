using Universidade.Infra.Repositories;
using Microsoft.EntityFrameworkCore;
using Universidade.Domain.Entities;
using Universidade.Teste.Configs;
using Universidade.Infra.Context;
using Moq.EntityFrameworkCore;
using AutoFixture;
using Xunit;
using Moq;

namespace Universidade.Teste.Sources.Infrastructure.Repositories
{
    [Trait("Repository", "Repository Curso")]
    public class CursoRepositoryTest
    {
        private readonly Mock<ManutençaoContext> _mockContext;
        private readonly Fixture _fixture;

        public CursoRepositoryTest()
        {
            _mockContext = new Mock<ManutençaoContext>(new DbContextOptionsBuilder<ManutençaoContext>().UseLazyLoadingProxies().Options);
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Cadastra Curso")]
        public async Task Post()
        {
            var entity = _fixture.Create<CursoEntity>();
            
            _mockContext.Setup(mock => mock.Set<CursoEntity>()).ReturnsDbSet(new List<CursoEntity> { new CursoEntity() });
            var repository = new CursoRepository(_mockContext.Object);

            try
            {
                await repository.AddAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Cadastra Curso")]
        public async Task PostNullException()
        {
            var entity = _fixture.Create<CursoEntity>();

            _mockContext.Setup(mock => mock.Set<CursoEntity>()).ReturnsDbSet(new List<CursoEntity>());

            var repository = new CursoRepository(_mockContext.Object);
            var exception = await Record.ExceptionAsync(() => repository.AddAsync(entity));
            
            Assert.Null(exception);
        }


        [Fact(DisplayName = "Cadastra Curso")]
        public async Task PostCursoNotNull()
        {
            var entity = _fixture.Create<CursoEntity>();

            _mockContext.Setup(mock => mock.Set<CursoEntity>()).ReturnsDbSet(new List<CursoEntity>());

            var repository = new CursoRepository(_mockContext.Object);
            Assert.NotNull(repository.AddAsync(entity));
        }


        [Fact(DisplayName = "Edita Curso Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<CursoEntity>();

            _mockContext.Setup(mock => mock.Set<CursoEntity>()).ReturnsDbSet(new List<CursoEntity> { new CursoEntity() });
            var repository = new CursoRepository(_mockContext.Object);

            try
            {
                await repository.EditAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Fact(DisplayName = "Lista Curso")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<CursoEntity>>();

            _mockContext.Setup(mock => mock.Set<CursoEntity>()).ReturnsDbSet(entities);

            var repository = new CursoRepository(_mockContext.Object);
            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Curso Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<CursoEntity>();

            _mockContext.Setup(mock => mock.Set<CursoEntity>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var repository = new CursoRepository(_mockContext.Object);
            var response = await repository.FindAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Curso Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<CursoEntity>();

            _mockContext.Setup(mock => mock.Set<CursoEntity>()).ReturnsDbSet(new List<CursoEntity> { entity });
            var repository = new CursoRepository(_mockContext.Object);

            try
            {
                await repository.RemoveAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
