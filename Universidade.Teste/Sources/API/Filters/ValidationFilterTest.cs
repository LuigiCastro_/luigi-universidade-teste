using Universidade.Api.Filters;
using Universidade.Domain.Enums;
using Universidade.Domain.Exceptions;
using Universidade.Teste.Configs;
using AutoFixture;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Xunit;
using Universidade.Domain.Contracts.Response;

namespace Universidade.Teste.Sources.Api.Filters
{
    public class ValidationFilterTest
    {
        private readonly Fixture _fixture;
        private readonly ActionContext _actionContext;
        private readonly List<IFilterMetadata> _filterMetadata;
        private readonly IActionResult _actionResult;
        private readonly ResultExecutionDelegate _resultExecutionDelegate;

        public ValidationFilterTest()
        {
            _fixture = FixtureConfig.Get();
            _actionContext = new ActionContext
            {
                ActionDescriptor = new ActionDescriptor(),
                HttpContext = new DefaultHttpContext(),
                RouteData = new RouteData()
            };
            _filterMetadata = new List<IFilterMetadata>();            
        }

        [Fact(DisplayName = "Teste ValidationFilter")]
        public async void OnValidation()
        {
            var validationContext = new ResultExecutingContext(_actionContext, _filterMetadata, 
                                                                _actionResult, _fixture)
            {
                Cancel = true
            };        
            
            var validationFilter = new ValidationFilter();
            var result = await Record.ExceptionAsync(() => validationFilter
            .OnResultExecutionAsync(validationContext, _resultExecutionDelegate));
            
            Assert.Null(result);
        }
    }
}