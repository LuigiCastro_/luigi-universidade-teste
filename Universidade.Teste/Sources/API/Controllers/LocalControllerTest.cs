using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Universidade.Api.Controller;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Interfaces.Service;
using Universidade.Teste.Configs;
using Xunit;

namespace Universidade.Teste.Sources.Api.Controllers;

[Trait("Controller", "Controller Local")]
public class LocalControllerTest
{
    private readonly Mock<ILocalService> _mockLocalService;
    private readonly Fixture _fixture;
    public LocalControllerTest()
    {
        _mockLocalService = new Mock<ILocalService>();
        _fixture = FixtureConfig.Get();
    }

    [Fact(DisplayName = "Obtem um endereço pelo CEP")]
    public async Task ObterDadosPorCep()
    {
        var response = _fixture.Create<LocalResponse>();
        _mockLocalService.Setup(mock => mock.ObterDadosPorCep(response.Cep)).ReturnsAsync(response);
        
        var controller = new LocalController(_mockLocalService.Object);
        var result = await controller.ObterDadosPorCep(response.Cep);            
        
        Assert.Equal(response.Cep, result.Cep);
    }


    [Fact(DisplayName = "Obtem uma lista de municipios")]
    public async Task ObterDadosPorMunicipio()
    {
        var response = _fixture.Create<LocalResponse>();
        _mockLocalService.Setup(mock => mock.ObterDadosPorMunicipio()).ReturnsAsync(response);
        
        var controller = new LocalController(_mockLocalService.Object);
        var result = await controller.ObterDadosPorMunicipio();            
        
        Assert.NotNull(result);
    }


    [Fact(DisplayName = "Obtem uma lista de estados")]
    public async Task ObterDadosPorEstado()
    {
        var response = _fixture.Create<LocalResponse>();
        _mockLocalService.Setup(mock => mock.ObterDadosPorEstado()).ReturnsAsync(response);
        
        var controller = new LocalController(_mockLocalService.Object);
        var result = await controller.ObterDadosPorEstado();            
        
        Assert.NotNull(result);
    }
}