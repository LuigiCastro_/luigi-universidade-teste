// using Universidade.Domain.Interfaces.Services;
// using Universidade.Domain.Contracts.Requests;
// using Universidade.Domain.Contracts.Response;
// using Universidade.Domain.Entities;
// using Universidade.Api.Controller;
// using Universidade.Teste.Configs;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using AutoFixture;
// using AutoMapper;
// using Xunit;
// using Moq;

// namespace Universidade.Teste.Sources.Api.Controllers
// {
//     [Trait("Controller", "Controller Aluno")]
//     public class AlunoControllerTest
//     {
//         private readonly Mock<IAlunoService> _mockAlunoService;
//         private readonly IMapper _mapper;
//         private readonly Fixture _fixture;

//         public AlunoControllerTest()
//         {
//             _mockAlunoService = new Mock<IAlunoService>();
//             _mapper = MapConfig.Get();
//             _fixture = FixtureConfig.Get();
//         }


//         [Fact(DisplayName = "Cadastra Aluno")]
//         public async Task Post()
//         {
//             var request = _fixture.Create<AlunoRequest>();

//             _mockAlunoService.Setup(mock => mock.AdicionarAsync(It.IsAny<AlunoEntity>())).Returns(Task.CompletedTask);

//             var controller = new AlunoController(_mapper, _mockAlunoService.Object);
//             var actionResult = await controller.PostAsync(request);
//             var objectResult = Assert.IsType<CreatedResult>(actionResult);
            
//             Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
//         }

//         [Fact(DisplayName = "Edita Aluno Existente")]
//         public async Task Put()
//         {
//             var id = _fixture.Create<int>();
//             var request = _fixture.Create<AlunoRequest>();

//             _mockAlunoService.Setup(mock => mock.AlterarAsync(It.IsAny<AlunoEntity>())).Returns(Task.CompletedTask);

//             var controller = new AlunoController(_mapper, _mockAlunoService.Object);
//             var actionResult = await controller.PutAsync(id, request);
//             var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
//             Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
//         }

//         [Fact(DisplayName = "Lista Aluno")]
//         public async Task GetAsync()
//         {
//             var entities = _fixture.Create<List<AlunoEntity>>();

//             _mockAlunoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

//             var controller = new AlunoController(_mapper, _mockAlunoService.Object);
//             var actionResult = await controller.GetAsync();
//             var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
//             var response = Assert.IsType<List<AlunoResponse>>(objectResult.Value);
            
//             Assert.True(response.Count() > 0);
//         }


//         [Fact(DisplayName = "Busca Aluno Id")]
//         public async Task GetById()
//         {
//             var entity = _fixture.Create<AlunoEntity>();

//             _mockAlunoService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

//             var controller = new AlunoController(_mapper, _mockAlunoService.Object);
//             var actionResult = await controller.GetByIdAsync(entity.Id);
//             var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
//             var response = Assert.IsType<AlunoResponse>(objectResult.Value);
            
//             Assert.Equal(response.Id, entity.Id);
//         }
        
        


//         [Fact(DisplayName = "Remove Aluno Existente")]
//         public async Task Delete()
//         {
//             var id = _fixture.Create<int>();

//             _mockAlunoService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

//             var controller = new AlunoController(_mapper, _mockAlunoService.Object);
//             var actionResult = await controller.DeleteAsync(id);
//             var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
//             Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
//         }


//         [Fact(DisplayName = "Busca Aluno pela matrícula")]
//         public async Task GetByMatricula()
//         {
//             var entity = _fixture.Create<AlunoEntity>();

//             _mockAlunoService.Setup(mock => mock.ObterAlunoPorMatriculaAsync(It.IsAny<string>())).ReturnsAsync(entity);

//             var controller = new AlunoController(_mapper, _mockAlunoService.Object);
//             var actionResult = await controller.GetByMatriculaAsync(entity.Matricula);
//             var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
//             var response = Assert.IsType<AlunoResponse>(objectResult.Value);

//             Assert.Equal(response.Matricula, entity.Matricula);
//         }


//         [Fact(DisplayName = "Atualiza a matrícula do Aluno")]
//         public async Task Patch()
//         {
//             var id = _fixture.Create<int>();
//             var request = _fixture.Create<AlunoMatriculaRequest>();

//             _mockAlunoService.Setup(mock => mock.AtualizarMatriculaAsync(id, request.Matricula)).Returns(Task.CompletedTask);

//             var controller = new AlunoController(_mapper, _mockAlunoService.Object);
//             var actionResult = await controller.PatchAsync(id, request);
//             var objectResult = Assert.IsType<OkResult>(actionResult);
            
//             Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
//         }
//     }
// }
