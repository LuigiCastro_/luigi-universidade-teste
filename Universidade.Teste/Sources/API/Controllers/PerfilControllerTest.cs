using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;
using Universidade.Api.Controller;
using Universidade.Teste.Configs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoFixture;
using AutoMapper;
using Xunit;
using Moq;

namespace Universidade.Teste.Sources.Api.Controllers
{
    [Trait("Controller", "Controller Perfil")]
    public class PerfilControllerTest
    {
        private readonly Mock<IPerfilService> _mockPerfilService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public PerfilControllerTest()
        {
            _mockPerfilService = new Mock<IPerfilService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Cadastra Perfil")]
        public async Task Post()
        {
            var request = _fixture.Create<PerfilRequest>();

            _mockPerfilService.Setup(mock => mock.AdicionarAsync(It.IsAny<PerfilEntity>())).Returns(Task.CompletedTask);

            var controller = new PerfilController(_mapper, _mockPerfilService.Object);
            var actionResult = await controller.PostAsync(request);
            var objectResult = Assert.IsType<CreatedResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Edita Perfil Existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<PerfilRequest>();

            _mockPerfilService.Setup(mock => mock.AlterarAsync(It.IsAny<PerfilEntity>())).Returns(Task.CompletedTask);

            var controller = new PerfilController(_mapper, _mockPerfilService.Object);
            var actionResult = await controller.PutAsync(id, request);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Lista Perfil")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<PerfilEntity>>();

            _mockPerfilService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new PerfilController(_mapper, _mockPerfilService.Object);
            var actionResult = await controller.GetAsync();
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<PerfilResponse>>(objectResult.Value);
            
            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Perfil Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<PerfilEntity>();

            _mockPerfilService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new PerfilController(_mapper, _mockPerfilService.Object);
            var actionResult = await controller.GetByIdAsync(entity.Id);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<PerfilResponse>(objectResult.Value);
            
            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Perfil Existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockPerfilService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new PerfilController(_mapper, _mockPerfilService.Object);
            var actionResult = await controller.DeleteAsync(id);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }
    }
}
