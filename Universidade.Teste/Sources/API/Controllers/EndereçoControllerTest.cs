using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;
using Universidade.Api.Controller;
using Universidade.Teste.Configs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoFixture;
using AutoMapper;
using Xunit;
using Moq;

namespace Universidade.Teste.Sources.Api.Controllers
{
    [Trait("Controller", "Controller Endereço")]
    public class EndereçoControllerTest
    {
        private readonly Mock<IEndereçoService> _mockEndereçoService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public EndereçoControllerTest()
        {
            _mockEndereçoService = new Mock<IEndereçoService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Cadastra Endereço")]
        public async Task Post()
        {
            var request = _fixture.Create<EndereçoRequest>();

            _mockEndereçoService.Setup(mock => mock.AdicionarAsync(It.IsAny<EndereçoEntity>())).Returns(Task.CompletedTask);

            var controller = new EndereçoController(_mapper, _mockEndereçoService.Object);
            var actionResult = await controller.PostAsync(request);
            var objectResult = Assert.IsType<CreatedResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Edita Endereço Existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<EndereçoRequest>();

            _mockEndereçoService.Setup(mock => mock.AlterarAsync(It.IsAny<EndereçoEntity>())).Returns(Task.CompletedTask);

            var controller = new EndereçoController(_mapper, _mockEndereçoService.Object);
            var actionResult = await controller.PutAsync(id, request);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Lista Endereço")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<EndereçoEntity>>();

            _mockEndereçoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new EndereçoController(_mapper, _mockEndereçoService.Object);
            var actionResult = await controller.GetAsync();
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<EndereçoResponse>>(objectResult.Value);
            
            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Endereço Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<EndereçoEntity>();

            _mockEndereçoService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new EndereçoController(_mapper, _mockEndereçoService.Object);
            var actionResult = await controller.GetByIdAsync(entity.Id);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<EndereçoResponse>(objectResult.Value);
            
            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Endereço Existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockEndereçoService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new EndereçoController(_mapper, _mockEndereçoService.Object);
            var actionResult = await controller.DeleteAsync(id);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Busca Endereço pelo Cep")]
        public async Task GetByCep()
        {
            var entity = _fixture.Create<EndereçoEntity>();

            _mockEndereçoService.Setup(mock => mock.ObterEndereçoPorCepAsync(It.IsAny<string>())).ReturnsAsync(entity);

            var controller = new EndereçoController(_mapper, _mockEndereçoService.Object);
            var actionResult = await controller.GetByCepAsync(entity.Cep);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<EndereçoResponse>(objectResult.Value);

            Assert.Equal(response.Cep, entity.Cep);
        }


        [Fact(DisplayName = "Atualiza o Complemento do Endereço")]
        public async Task Patch()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<EndereçoComplementoRequest>();

            _mockEndereçoService.Setup(mock => mock.AtualizarComplementoAsync(id, request.Complemento)).Returns(Task.CompletedTask);

            var controller = new EndereçoController(_mapper, _mockEndereçoService.Object);
            var actionResult = await controller.PatchAsync(id, request);
            var objectResult = Assert.IsType<OkResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
    }
}
