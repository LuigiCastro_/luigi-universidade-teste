using Universidade.Api.Controller;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Services;
using Universidade.Teste.Configs;
using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Universidade.Teste.Sources.Api.Controllers
{
    [Trait("Controller", "Controller Usuarios")]
    public class AutenticacoesControllerTest
    {
        private readonly Mock<IUsuarioService> _mockUsuarioService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public AutenticacoesControllerTest()
        {
            _mockUsuarioService = new Mock<IUsuarioService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Cria Token")]
        public async Task Post()
        {
            var request = _fixture.Create<AutenticaçaoRequest>();
            var response = _fixture.Create<AutenticaçaoResponse>();

            _mockUsuarioService.Setup(mock => mock.AutenticarUsuarioAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(response);

            var controller = new AutenticaçaoController(_mockUsuarioService.Object);
            var actionResult = await controller.PostAsync(request);

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var responseResult = Assert.IsType<AutenticaçaoResponse>(objectResult.Value);
            
            Assert.Equal(responseResult.Token, response.Token);
        }
    }
}
