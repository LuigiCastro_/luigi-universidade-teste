using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;
using Universidade.Api.Controller;
using Universidade.Teste.Configs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoFixture;
using AutoMapper;
using Xunit;
using Moq;

namespace Universidade.Teste.Sources.Api.Controllers
{
    [Trait("Controller", "Controller Curso")]
    public class CursoControllerTest
    {
        private readonly Mock<ICursoService> _mockCursoService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public CursoControllerTest()
        {
            _mockCursoService = new Mock<ICursoService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Cadastra Curso")]
        public async Task Post()
        {
            var request = _fixture.Create<CursoRequest>();

            _mockCursoService.Setup(mock => mock.AdicionarAsync(It.IsAny<CursoEntity>())).Returns(Task.CompletedTask);

            var controller = new CursoController(_mapper, _mockCursoService.Object);
            var actionResult = await controller.PostAsync(request);
            var objectResult = Assert.IsType<CreatedResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Edita Curso Existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<CursoRequest>();

            _mockCursoService.Setup(mock => mock.AlterarAsync(It.IsAny<CursoEntity>())).Returns(Task.CompletedTask);

            var controller = new CursoController(_mapper, _mockCursoService.Object);
            var actionResult = await controller.PutAsync(id, request);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Lista Curso")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<CursoEntity>>();

            _mockCursoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new CursoController(_mapper, _mockCursoService.Object);
            var actionResult = await controller.GetAsync();
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<CursoResponse>>(objectResult.Value);
            
            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Curso Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<CursoEntity>();

            _mockCursoService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new CursoController(_mapper, _mockCursoService.Object);
            var actionResult = await controller.GetByIdAsync(entity.Id);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<CursoResponse>(objectResult.Value);
            
            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Curso Existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockCursoService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new CursoController(_mapper, _mockCursoService.Object);
            var actionResult = await controller.DeleteAsync(id);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Busca Curso pelo codigo")]
        public async Task GetByCodigo()
        {
            var entity = _fixture.Create<CursoEntity>();

            _mockCursoService.Setup(mock => mock.ObterCursoPorCodigoAsync(It.IsAny<string>())).ReturnsAsync(entity);

            var controller = new CursoController(_mapper, _mockCursoService.Object);
            var actionResult = await controller.GetByCodigoAsync(entity.Codigo);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<CursoResponse>(objectResult.Value);

            Assert.Equal(response.Codigo, entity.Codigo);
        }


        [Fact(DisplayName = "Atualiza o turno do Curso")]
        public async Task Patch()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<CursoTurnoRequest>();

            _mockCursoService.Setup(mock => mock.AtualizarTurnoAsync(id, request.Turno)).Returns(Task.CompletedTask);

            var controller = new CursoController(_mapper, _mockCursoService.Object);
            var actionResult = await controller.PatchAsync(id, request);
            var objectResult = Assert.IsType<OkResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
    }
}
