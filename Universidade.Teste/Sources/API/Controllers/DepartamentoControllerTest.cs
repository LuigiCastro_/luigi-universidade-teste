using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;
using Universidade.Api.Controller;
using Universidade.Teste.Configs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoFixture;
using AutoMapper;
using Xunit;
using Moq;

namespace Universidade.Teste.Sources.Api.Controllers
{
    [Trait("Controller", "Controller Departamento")]
    public class DepartamentoControllerTest
    {
        private readonly Mock<IDepartamentoService> _mockDepartamentoService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public DepartamentoControllerTest()
        {
            _mockDepartamentoService = new Mock<IDepartamentoService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Cadastra Departamento")]
        public async Task Post()
        {
            var request = _fixture.Create<DepartamentoRequest>();

            _mockDepartamentoService.Setup(mock => mock.AdicionarAsync(It.IsAny<DepartamentoEntity>())).Returns(Task.CompletedTask);

            var controller = new DepartamentoController(_mapper, _mockDepartamentoService.Object);
            var actionResult = await controller.PostAsync(request);
            var objectResult = Assert.IsType<CreatedResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Edita Departamento Existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<DepartamentoRequest>();

            _mockDepartamentoService.Setup(mock => mock.AlterarAsync(It.IsAny<DepartamentoEntity>())).Returns(Task.CompletedTask);

            var controller = new DepartamentoController(_mapper, _mockDepartamentoService.Object);
            var actionResult = await controller.PutAsync(id, request);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Lista Departamento")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<DepartamentoEntity>>();

            _mockDepartamentoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new DepartamentoController(_mapper, _mockDepartamentoService.Object);
            var actionResult = await controller.GetAsync();
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<DepartamentoResponse>>(objectResult.Value);
            
            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Departamento Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<DepartamentoEntity>();

            _mockDepartamentoService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new DepartamentoController(_mapper, _mockDepartamentoService.Object);
            var actionResult = await controller.GetByIdAsync(entity.Id);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<DepartamentoResponse>(objectResult.Value);
            
            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Departamento Existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockDepartamentoService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new DepartamentoController(_mapper, _mockDepartamentoService.Object);
            var actionResult = await controller.DeleteAsync(id);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Busca Departamento pelo codigo")]
        public async Task GetByCodigo()
        {
            var entity = _fixture.Create<DepartamentoEntity>();

            _mockDepartamentoService.Setup(mock => mock.ObterDepartamentoPorCodigoAsync(It.IsAny<string>())).ReturnsAsync(entity);

            var controller = new DepartamentoController(_mapper, _mockDepartamentoService.Object);
            var actionResult = await controller.GetByCodigoAsync(entity.Codigo);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<DepartamentoResponse>(objectResult.Value);

            Assert.Equal(response.Codigo, entity.Codigo);
        }


        [Fact(DisplayName = "Atualiza o Nome do Departamento")]
        public async Task Patch()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<DepartamentoNomeRequest>();

            _mockDepartamentoService.Setup(mock => mock.AtualizarNomeAsync(id, request.Nome)).Returns(Task.CompletedTask);

            var controller = new DepartamentoController(_mapper, _mockDepartamentoService.Object);
            var actionResult = await controller.PatchAsync(id, request);
            var objectResult = Assert.IsType<OkResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
    }
}
