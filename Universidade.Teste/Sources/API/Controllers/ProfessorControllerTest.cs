// using Universidade.Domain.Interfaces.Services;
// using Universidade.Domain.Contracts.Requests;
// using Universidade.Domain.Contracts.Response;
// using Universidade.Domain.Entities;
// using Universidade.Api.Controller;
// using Universidade.Teste.Configs;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using AutoFixture;
// using AutoMapper;
// using Xunit;
// using Moq;

// namespace Universidade.Teste.Sources.Api.Controllers
// {
//     [Trait("Controller", "Controller Professor")]
//     public class ProfessorControllerTest
//     {
//         private readonly Mock<IProfessorService> _mockProfessorService;
//         private readonly IMapper _mapper;
//         private readonly Fixture _fixture;

//         public ProfessorControllerTest()
//         {
//             _mockProfessorService = new Mock<IProfessorService>();
//             _mapper = MapConfig.Get();
//             _fixture = FixtureConfig.Get();
//         }


//         [Fact(DisplayName = "Cadastra Professor")]
//         public async Task Post()
//         {
//             var request = _fixture.Create<ProfessorRequest>();

//             _mockProfessorService.Setup(mock => mock.AdicionarAsync(It.IsAny<ProfessorEntity>())).Returns(Task.CompletedTask);

//             var controller = new ProfessorController(_mapper, _mockProfessorService.Object);
//             var actionResult = await controller.PostAsync(request);
//             var objectResult = Assert.IsType<CreatedResult>(actionResult);
            
//             Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
//         }

//         [Fact(DisplayName = "Edita Professor Existente")]
//         public async Task Put()
//         {
//             var id = _fixture.Create<int>();
//             var request = _fixture.Create<ProfessorRequest>();

//             _mockProfessorService.Setup(mock => mock.AlterarAsync(It.IsAny<ProfessorEntity>())).Returns(Task.CompletedTask);

//             var controller = new ProfessorController(_mapper, _mockProfessorService.Object);
//             var actionResult = await controller.PutAsync(id, request);
//             var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
//             Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
//         }

//         [Fact(DisplayName = "Lista Professor")]
//         public async Task GetAsync()
//         {
//             var entities = _fixture.Create<List<ProfessorEntity>>();

//             _mockProfessorService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

//             var controller = new ProfessorController(_mapper, _mockProfessorService.Object);
//             var actionResult = await controller.GetAsync();
//             var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
//             var response = Assert.IsType<List<ProfessorResponse>>(objectResult.Value);
            
//             Assert.True(response.Count() > 0);
//         }


//         [Fact(DisplayName = "Busca Professor Id")]
//         public async Task GetById()
//         {
//             var entity = _fixture.Create<ProfessorEntity>();

//             _mockProfessorService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

//             var controller = new ProfessorController(_mapper, _mockProfessorService.Object);
//             var actionResult = await controller.GetByIdAsync(entity.Id);
//             var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
//             var response = Assert.IsType<ProfessorResponse>(objectResult.Value);
            
//             Assert.Equal(response.Id, entity.Id);
//         }


//         [Fact(DisplayName = "Remove Professor Existente")]
//         public async Task Delete()
//         {
//             var id = _fixture.Create<int>();

//             _mockProfessorService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

//             var controller = new ProfessorController(_mapper, _mockProfessorService.Object);
//             var actionResult = await controller.DeleteAsync(id);
//             var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
//             Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
//         }


//         [Fact(DisplayName = "Busca Professor pelo Cpf")]
//         public async Task GetByCpf()
//         {
//             var entity = _fixture.Create<ProfessorEntity>();

//             _mockProfessorService.Setup(mock => mock.ObterProfessorPorCpfAsync(It.IsAny<string>())).ReturnsAsync(entity);

//             var controller = new ProfessorController(_mapper, _mockProfessorService.Object);
//             var actionResult = await controller.GetByCpfAsync(entity.Cpf);
//             var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
//             var response = Assert.IsType<ProfessorResponse>(objectResult.Value);

//             Assert.Equal(response.Cpf, entity.Cpf);
//         }


//         [Fact(DisplayName = "Atualiza o Nome do Professor")]
//         public async Task Patch()
//         {
//             var id = _fixture.Create<int>();
//             var request = _fixture.Create<ProfessorNomeRequest>();

//             _mockProfessorService.Setup(mock => mock.AtualizarNomeAsync(id, request.Nome)).Returns(Task.CompletedTask);

//             var controller = new ProfessorController(_mapper, _mockProfessorService.Object);
//             var actionResult = await controller.PatchAsync(id, request);
//             var objectResult = Assert.IsType<OkResult>(actionResult);
            
//             Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
//         }
//     }
// }
