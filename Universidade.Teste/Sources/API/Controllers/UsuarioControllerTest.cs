using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;
using Universidade.Api.Controller;
using Universidade.Teste.Configs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoFixture;
using AutoMapper;
using Xunit;
using Moq;
using System.Linq.Expressions;

namespace Universidade.Teste.Sources.Api.Controllers
{
    [Trait("Controller", "Controller Usuarios")]
    public class UsuarioControllerTest
    {
        private readonly Mock<IUsuarioService> _mockUsuarioService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public UsuarioControllerTest()
        {
            _mockUsuarioService = new Mock<IUsuarioService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }
        

        [Fact(DisplayName = "Cadastra Usuario")]
        public async Task Post()
        {
            var request = _fixture.Create<UsuarioRequest>();

            _mockUsuarioService.Setup(mock => mock.AdicionarAsync(It.IsAny<UsuarioEntity>())).Returns(Task.CompletedTask);

            var controller = new UsuarioController(_mapper, _mockUsuarioService.Object);
            var actionResult = await controller.PostAsync(request);
            var objectResult = Assert.IsType<CreatedResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Edita Usuario Existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<UsuarioRequest>();

            _mockUsuarioService.Setup(mock => mock.AlterarAsync(It.IsAny<UsuarioEntity>())).Returns(Task.CompletedTask);

            var controller = new UsuarioController(_mapper, _mockUsuarioService.Object);
            var actionResult = await controller.PutAsync(id, request);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Lista Usuarios")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<UsuarioEntity>>();

            _mockUsuarioService.Setup(mock => mock.ObterTodosUsuarioAsync()).ReturnsAsync(entities);

            var controller = new UsuarioController(_mapper, _mockUsuarioService.Object);
            var actionResult = await controller.GetAsync();
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<UsuarioResponse>>(objectResult.Value);
            
            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Busca Usuario Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<UsuarioEntity>();

            _mockUsuarioService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new UsuarioController(_mapper, _mockUsuarioService.Object);
            var actionResult = await controller.GetByIdAsync(entity.Id);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<UsuarioResponse>(objectResult.Value);
            
            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Remove Usuario Existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockUsuarioService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new UsuarioController(_mapper, _mockUsuarioService.Object);
            var actionResult = await controller.DeleteAsync(id);
            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Busca Usuario pelo Nome")]
        public async Task GetByName()
        {
            var entity = _fixture.Create<UsuarioEntity>();

            _mockUsuarioService.Setup(mock => mock.ObterAsync(It.IsAny<Expression<Func<UsuarioEntity, bool>>>())).ReturnsAsync(entity);

            var controller = new UsuarioController(_mapper, _mockUsuarioService.Object);
            var actionResult = await controller.GetByNameAsync(entity.Nome);
            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<UsuarioResponse>(objectResult.Value);
            
            Assert.Equal(response.Nome, entity.Nome);
        }


        [Fact(DisplayName = "Atualiza o Telefone do Usuario")]
        public async Task Patch()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<UsuarioTelefoneRequest>();

            _mockUsuarioService.Setup(mock => mock.AtualizarTelefoneAsync(id, request.Telefone)).Returns(Task.CompletedTask);

            var controller = new UsuarioController(_mapper, _mockUsuarioService.Object);
            var actionResult = await controller.PatchAsync(id, request);
            var objectResult = Assert.IsType<OkResult>(actionResult);
            
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
    }
}
