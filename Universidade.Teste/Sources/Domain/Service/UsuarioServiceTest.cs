using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;
using Universidade.Domain.Service;
using Universidade.Domain.Settings;
using Universidade.Teste.Configs;
using AutoFixture;
using Bogus;
using Microsoft.AspNetCore.Http;
using Moq;
using System.Linq.Expressions;
using System.Security.Claims;
using Xunit;
using Universidade.Domain.Interfaces.Services;
using Universidade.Api.Controller;
using AutoMapper;

namespace Universidade.Teste.Sources.Domain.Services
{
    [Trait("Service", "Service Usuario")]
    public class UsuarioServiceTest
    {
        private readonly Mock<IUsuarioRepository> _mockUsuarioRepository;
        private readonly Mock<IUsuarioService> _mockUsuarioService;
        private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
        private readonly Mock<AppSettings> _mockAppSettings;
        private readonly Faker _faker;
        private readonly Fixture _fixture;
        private readonly IMapper _mapper;

        public UsuarioServiceTest()
        {
            _mockUsuarioRepository = new Mock<IUsuarioRepository>();
            _mockUsuarioService = new Mock<IUsuarioService>();
            _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            _faker = new Faker();
            _fixture = FixtureConfig.Get();
            _mockAppSettings = new Mock<AppSettings>();
            _mapper = MapConfig.Get();
        }

        [Fact(DisplayName = "Cadastra Usuario")]
        public async Task Post()
        {
            var entity = _fixture.Create<UsuarioEntity>();

            _mockUsuarioRepository.Setup(mock => mock.AddAsync(It.IsAny<UsuarioEntity>())).Returns(Task.CompletedTask);

            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AdicionarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Cadastra Usuario")]
        public async Task PostUsuario()
        {
            var entity = _fixture.Create<UsuarioEntity>();
            entity.Senha = BCrypt.Net.BCrypt.HashPassword(entity.Senha, BCrypt.Net.BCrypt.GenerateSalt());
            
            _mockUsuarioRepository.Setup(mock => mock.AddAsync(It.IsAny<UsuarioEntity>())).Returns(Task.CompletedTask);
        
            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);

            Assert.NotNull(service.CriarUsuarioAsync(entity));
        }


        [Theory(DisplayName = "Edita Usuario")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task PutUsuario(string perfil)
        {
            var entity = _fixture.Create<UsuarioEntity>();
            entity.Senha = BCrypt.Net.BCrypt.HashPassword(entity.Senha, BCrypt.Net.BCrypt.GenerateSalt());
            
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

            _mockUsuarioRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<UsuarioEntity, bool>>>())).ReturnsAsync(entity);
            _mockUsuarioRepository.Setup(mock => mock.EditAsync(It.IsAny<UsuarioEntity>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);
        
            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);
            
            Assert.NotNull(service.AtualizarUsuarioAsync(entity));
        }


        [Theory(DisplayName = "Edita Usuario Existente")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task Put(string perfil)
         {
            var entity = _fixture.Create<UsuarioEntity>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

            _mockUsuarioRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<UsuarioEntity, bool>>>())).ReturnsAsync(entity);
            _mockUsuarioRepository.Setup(mock => mock.EditAsync(It.IsAny<UsuarioEntity>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AlterarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Theory(DisplayName = "Edita Telefone de um Usuario Existente")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task PutTelefone(string perfil)
         {
            var entity = _fixture.Create<UsuarioEntity>();
            var fakerPhone = _faker.Person.Phone;

            _mockUsuarioRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<UsuarioEntity, bool>>>())).ReturnsAsync(entity);
            _mockUsuarioRepository.Setup(mock => mock.EditAsync(It.IsAny<UsuarioEntity>())).Returns(Task.CompletedTask);

            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AtualizarTelefoneAsync(entity.Id, fakerPhone);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
        

        [Theory(DisplayName = "Lista Usuarios")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task Get(string perfil)
        {
            var entities = _fixture.Create<List<UsuarioEntity>>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);
            
            _mockUsuarioRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<UsuarioEntity, bool>>>())).ReturnsAsync(entities);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterTodosAsync();

            Assert.True(response.ToList().Count() > 0);
        }

        [Theory(DisplayName = "Busca Usuario Id")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task GetById(string perfil)
        {
            var entity = _fixture.Create<UsuarioEntity>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

            _mockUsuarioRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<UsuarioEntity, bool>>>())).ReturnsAsync(entity);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterPorIdAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }


        [Theory(DisplayName = "Lista Usuarios")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task GetUsuario(string perfil)
        {
            var entities = _fixture.Create<List<UsuarioEntity>>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);
            
            _mockUsuarioRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<UsuarioEntity, bool>>>())).ReturnsAsync(entities);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);
            
            Assert.NotNull(service.ObterTodosUsuarioAsync());
        }


        [Theory(DisplayName = "Busca Usuario Id")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task GetByIdUsuario(string perfil)
        {
            var entity = _fixture.Create<UsuarioEntity>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

            _mockUsuarioRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<UsuarioEntity, bool>>>())).ReturnsAsync(entity);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterUsuarioPorIdAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }


        [Theory(DisplayName = "Remove Usuario Existente")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task Delete(string perfil)
        {
            var entity = _fixture.Create<UsuarioEntity>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

            _mockUsuarioRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
            _mockUsuarioRepository.Setup(mock => mock.RemoveAsync(It.IsAny<UsuarioEntity>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new UsuarioService(_mockUsuarioRepository.Object, _mockAppSettings.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.DeletarAsync(entity.Id);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
