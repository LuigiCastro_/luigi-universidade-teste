using Universidade.Domain.Contracts.Http;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Universidade.Teste.Fakers;
using Universidade.Domain.Service.Local;

namespace Universidade.Teste.Sources.Domain.Services
{
    public class LocalServiceTeste
    {
        private readonly Mock<IHttpClientFactory> _httpClientFactory = new Mock<IHttpClientFactory>();
        private readonly Mock<HttpMessageHandler> _httpMessageHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);

        [Fact(DisplayName = "Lista todos os Usuarios")]
        public async Task ObterDadosPorCep()
        {
            var local = LocalResponseFaker.Local();
            var json = JsonConvert.SerializeObject(local);

            _httpMessageHandler.Protected()
                   .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                   .ReturnsAsync(new HttpResponseMessage
                   {
                       StatusCode = HttpStatusCode.OK,
                       Content = new StringContent(json),
                   });

            var client = new HttpClient(_httpMessageHandler.Object);

            _httpClientFactory.Setup(mock => mock.CreateClient(It.IsAny<string>())).Returns(client);

            IOptions<HttpCepOptions> HttpCepOptions = Options.Create(HttpOptionsFaker.CepOptions());
            IOptions<HttpMunicipioOptions> HttpMunicipioOptions = Options.Create(new HttpMunicipioOptions());
            IOptions<HttpEstadoOptions> HttpEstadoOptions = Options.Create(new HttpEstadoOptions());


            var service = new LocalService(_httpClientFactory.Object,
                                           HttpCepOptions,
                                           HttpMunicipioOptions,
                                           HttpEstadoOptions);

            var response = await service.ObterDadosPorCep(local.Cep);

            Assert.True(local.Cep == response.Cep);
        }
    }
}
