using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;
using Universidade.Domain.Service;
using Universidade.Domain.Settings;
using Universidade.Teste.Configs;
using AutoFixture;
using Bogus;
using Microsoft.AspNetCore.Http;
using Moq;
using System.Linq.Expressions;
using System.Security.Claims;
using Xunit;

namespace Universidade.Teste.Sources.Domain.Services
{
    [Trait("Service", "Service Perfil")]
    public class PerfilServiceTest
    {
        private readonly Mock<IPerfilRepository> _mockPerfilRepository;
        private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
        private readonly Faker _faker;
        private readonly Fixture _fixture;

        public PerfilServiceTest()
        {
            _mockPerfilRepository = new Mock<IPerfilRepository>();
            _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            _faker = new Faker();
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Cadastra Perfil")]
        public async Task Post()
        {
            var entity = _fixture.Create<PerfilEntity>();

            _mockPerfilRepository.Setup(mock => mock.AddAsync(It.IsAny<PerfilEntity>())).Returns(Task.CompletedTask);

            var service = new PerfilService(_mockPerfilRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AdicionarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }


        [Theory(DisplayName = "Edita Perfil Existente")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task Put(string perfil)
         {
            var entity = _fixture.Create<PerfilEntity>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

            _mockPerfilRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<PerfilEntity, bool>>>())).ReturnsAsync(entity);
            _mockPerfilRepository.Setup(mock => mock.EditAsync(It.IsAny<PerfilEntity>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new PerfilService(_mockPerfilRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AlterarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        
        [Theory(DisplayName = "Lista Perfils")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task Get(string perfil)
        {
            var entities = _fixture.Create<List<PerfilEntity>>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);
            
            _mockPerfilRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<PerfilEntity, bool>>>())).ReturnsAsync(entities);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new PerfilService(_mockPerfilRepository.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterTodosAsync();

            Assert.True(response.ToList().Count() > 0);
        }


        [Theory(DisplayName = "Busca Perfil Id")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task GetById(string perfil)
        {
            var entity = _fixture.Create<PerfilEntity>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

            _mockPerfilRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<PerfilEntity, bool>>>())).ReturnsAsync(entity);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new PerfilService(_mockPerfilRepository.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterPorIdAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }


        [Theory(DisplayName = "Remove Perfil Existente")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task Delete(string perfil)
        {
            var entity = _fixture.Create<PerfilEntity>();
            var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

            _mockPerfilRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
            _mockPerfilRepository.Setup(mock => mock.RemoveAsync(It.IsAny<PerfilEntity>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new PerfilService(_mockPerfilRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.DeletarAsync(entity.Id);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
