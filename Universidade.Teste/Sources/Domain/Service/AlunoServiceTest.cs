// using Universidade.Domain.Entities;
// using Universidade.Domain.Interfaces.Repositories;
// using Universidade.Domain.Service;
// using Universidade.Domain.Settings;
// using Universidade.Teste.Configs;
// using AutoFixture;
// using Bogus;
// using Microsoft.AspNetCore.Http;
// using Moq;
// using System.Linq.Expressions;
// using System.Security.Claims;
// using Xunit;

// namespace Universidade.Teste.Sources.Domain.Services
// {
//     [Trait("Service", "Service Aluno")]
//     public class AlunoServiceTest
//     {
//         private readonly Mock<IAlunoRepository> _mockAlunoRepository;
//         private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
//         private readonly Faker _faker;
//         private readonly Fixture _fixture;

//         public AlunoServiceTest()
//         {
//             _mockAlunoRepository = new Mock<IAlunoRepository>();
//             _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
//             _faker = new Faker();
//             _fixture = FixtureConfig.Get();
//         }

//         [Fact(DisplayName = "Cadastra Aluno")]
//         public async Task Post()
//         {
//             var entity = _fixture.Create<AlunoEntity>();

//             _mockAlunoRepository.Setup(mock => mock.AddAsync(It.IsAny<AlunoEntity>())).Returns(Task.CompletedTask);

//             var service = new AlunoService(_mockAlunoRepository.Object, _mockHttpContextAccessor.Object);

//             try
//             {
//                 await service.AdicionarAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }


//         [Theory(DisplayName = "Edita Aluno Existente")]
//         [InlineData("Cliente")]
//         [InlineData("Tecnico")]
//         public async Task Put(string perfil)
//          {
//             var entity = _fixture.Create<AlunoEntity>();
//             var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

//             _mockAlunoRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<AlunoEntity, bool>>>())).ReturnsAsync(entity);
//             _mockAlunoRepository.Setup(mock => mock.EditAsync(It.IsAny<AlunoEntity>())).Returns(Task.CompletedTask);
//             _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

//             var service = new AlunoService(_mockAlunoRepository.Object, _mockHttpContextAccessor.Object);

//             try
//             {
//                 await service.AlterarAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }

        
//         [Theory(DisplayName = "Lista Alunos")]
//         [InlineData("Cliente")]
//         [InlineData("Tecnico")]
//         public async Task Get(string perfil)
//         {
//             var entities = _fixture.Create<List<AlunoEntity>>();
//             var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);
            
//             _mockAlunoRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<AlunoEntity, bool>>>())).ReturnsAsync(entities);
//             _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

//             var service = new AlunoService(_mockAlunoRepository.Object, _mockHttpContextAccessor.Object);

//             var response = await service.ObterTodosAsync();

//             Assert.True(response.ToList().Count() > 0);
//         }


//         [Theory(DisplayName = "Busca Aluno Id")]
//         [InlineData("Cliente")]
//         [InlineData("Tecnico")]
//         public async Task GetById(string perfil)
//         {
//             var entity = _fixture.Create<AlunoEntity>();
//             var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

//             _mockAlunoRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<AlunoEntity, bool>>>())).ReturnsAsync(entity);
//             _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

//             var service = new AlunoService(_mockAlunoRepository.Object, _mockHttpContextAccessor.Object);

//             var response = await service.ObterPorIdAsync(entity.Id);

//             Assert.Equal(response.Id, entity.Id);
//         }


//         [Theory(DisplayName = "Remove Aluno Existente")]
//         [InlineData("Cliente")]
//         [InlineData("Tecnico")]
//         public async Task Delete(string perfil)
//         {
//             var entity = _fixture.Create<AlunoEntity>();
//             var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

//             _mockAlunoRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
//             _mockAlunoRepository.Setup(mock => mock.RemoveAsync(It.IsAny<AlunoEntity>())).Returns(Task.CompletedTask);
//             _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

//             var service = new AlunoService(_mockAlunoRepository.Object, _mockHttpContextAccessor.Object);

//             try
//             {
//                 await service.DeletarAsync(entity.Id);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }
//     }
// }
