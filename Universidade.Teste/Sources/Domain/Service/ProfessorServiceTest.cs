// using Universidade.Domain.Entities;
// using Universidade.Domain.Interfaces.Repositories;
// using Universidade.Domain.Service;
// using Universidade.Domain.Settings;
// using Universidade.Teste.Configs;
// using AutoFixture;
// using Bogus;
// using Microsoft.AspNetCore.Http;
// using Moq;
// using System.Linq.Expressions;
// using System.Security.Claims;
// using Xunit;

// namespace Universidade.Teste.Sources.Domain.Services
// {
//     [Trait("Service", "Service Professor")]
//     public class ProfessorServiceTest
//     {
//         private readonly Mock<IProfessorRepository> _mockProfessorRepository;
//         private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
//         private readonly Faker _faker;
//         private readonly Fixture _fixture;

//         public ProfessorServiceTest()
//         {
//             _mockProfessorRepository = new Mock<IProfessorRepository>();
//             _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
//             _faker = new Faker();
//             _fixture = FixtureConfig.Get();
//         }

//         [Fact(DisplayName = "Cadastra Professor")]
//         public async Task Post()
//         {
//             var entity = _fixture.Create<ProfessorEntity>();

//             _mockProfessorRepository.Setup(mock => mock.AddAsync(It.IsAny<ProfessorEntity>())).Returns(Task.CompletedTask);

//             var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);

//             try
//             {
//                 await service.AdicionarAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }


//         [Theory(DisplayName = "Edita Professor Existente")]
//         [InlineData("Cliente")]
//         [InlineData("Tecnico")]
//         public async Task Put(string perfil)
//          {
//             var entity = _fixture.Create<ProfessorEntity>();
//             var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

//             _mockProfessorRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<ProfessorEntity, bool>>>())).ReturnsAsync(entity);
//             _mockProfessorRepository.Setup(mock => mock.EditAsync(It.IsAny<ProfessorEntity>())).Returns(Task.CompletedTask);
//             _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

//             var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);

//             try
//             {
//                 await service.AlterarAsync(entity);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }

        
//         [Theory(DisplayName = "Lista Professors")]
//         [InlineData("Cliente")]
//         [InlineData("Tecnico")]
//         public async Task Get(string perfil)
//         {
//             var entities = _fixture.Create<List<ProfessorEntity>>();
//             var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);
            
//             _mockProfessorRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<ProfessorEntity, bool>>>())).ReturnsAsync(entities);
//             _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

//             var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);

//             var response = await service.ObterTodosAsync();

//             Assert.True(response.ToList().Count() > 0);
//         }


//         [Theory(DisplayName = "Busca Professor Id")]
//         [InlineData("Cliente")]
//         [InlineData("Tecnico")]
//         public async Task GetById(string perfil)
//         {
//             var entity = _fixture.Create<ProfessorEntity>();
//             var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

//             _mockProfessorRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<ProfessorEntity, bool>>>())).ReturnsAsync(entity);
//             _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

//             var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);

//             var response = await service.ObterPorIdAsync(entity.Id);

//             Assert.Equal(response.Id, entity.Id);
//         }


//         [Theory(DisplayName = "Remove Professor Existente")]
//         [InlineData("Cliente")]
//         [InlineData("Tecnico")]
//         public async Task Delete(string perfil)
//         {
//             var entity = _fixture.Create<ProfessorEntity>();
//             var claims = ClaimConfig.Get(_faker.UniqueIndex, _faker.Person.FullName, _faker.Person.Email, perfil);

//             _mockProfessorRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
//             _mockProfessorRepository.Setup(mock => mock.RemoveAsync(It.IsAny<ProfessorEntity>())).Returns(Task.CompletedTask);
//             _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

//             var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);

//             try
//             {
//                 await service.DeletarAsync(entity.Id);
//             }
//             catch (Exception)
//             {
//                 Assert.True(false);
//             }
//         }
//     }
// }
