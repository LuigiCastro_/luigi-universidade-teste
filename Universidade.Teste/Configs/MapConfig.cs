using AutoMapper;

namespace Universidade.Teste.Configs
{
    public static class MapConfig
    {
        public static IMapper Get()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new Universidade.Api.Profiles.MappingProfile());
            });

            return mockMapper.CreateMapper();
        }
    }
}
