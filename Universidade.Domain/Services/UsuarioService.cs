using System.Linq.Expressions;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Interfaces.Services;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Universidade.Domain.Settings;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Http;
using Universidade.Domain.Utils;
using Universidade.Domain.Exceptions;


namespace Universidade.Domain.Service;

public class UsuarioService : BaseService<UsuarioEntity>, IUsuarioService
{
    private readonly IUsuarioRepository _repository;
    private readonly AppSettings _appSettings;


    public UsuarioService(IUsuarioRepository usuarioRepository, AppSettings appSettings, IHttpContextAccessor httpContextAccessor) : base(usuarioRepository, httpContextAccessor)
    {
        _repository = usuarioRepository;
        _appSettings = appSettings;
    }

    public async Task CriarUsuarioAsync(UsuarioEntity usuarioEntity)
    {
        usuarioEntity.Senha = BCrypt.Net.BCrypt.HashPassword(usuarioEntity.Senha, BCrypt.Net.BCrypt.GenerateSalt());
        await AdicionarAsync(usuarioEntity);
    }
    public async Task AtualizarUsuarioAsync(UsuarioEntity usuarioEntity)
    {
        usuarioEntity.Senha = BCrypt.Net.BCrypt.HashPassword(usuarioEntity.Senha, BCrypt.Net.BCrypt.GenerateSalt());
        await AlterarAsync(usuarioEntity);
    }

    public async Task AtualizarTelefoneAsync(int id, string telefone)
    {
        var entity = await ObterPorIdAsync(id);
        entity.Telefone = telefone;
        entity.DataAlteracao = DateTime.Now;
        entity.UsuarioAlteracao = UserId;
        await _repository.EditAsync(entity);
        
    }
    
    public async Task<List<UsuarioEntity>> ObterTodosUsuarioAsync()
    {
        if (UserPerfil == ConstanteUtil.PerfilLogadoNome)
                return await ObterTodosAsync(x => x.Ativo && x.Id == UserId);
        else
            return await ObterTodosAsync();
    }
 
    public async Task<UsuarioEntity> ObterUsuarioPorIdAsync(int id)
    {
        if (UserPerfil == ConstanteUtil.PerfilLogadoNome)
            return await ObterAsync(x => x.Id == id && x.Ativo && x.Id == UserId);
        else
            return await ObterPorIdAsync(id);
    }
    

    public async Task<AutenticaçaoResponse> AutenticarUsuarioAsync(string email, string senha)
    {
        var entity = await ObterAsync(x => x.Email == email && x.Ativo);

            if (!BCrypt.Net.BCrypt.Verify(senha, entity.Senha))
                throw new InformacaoException(Enums.StatusException.FormatoIncorreto, "Usuário ou senha incorreta");

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, entity.Id.ToString()),
                    new Claim(ClaimTypes.Name, entity.Nome),
                    new Claim(ClaimTypes.Email, entity.Email),
                    new Claim(ClaimTypes.Role, entity.Perfil.Nome)
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appSettings.JwtSecurityKey)),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return new AutenticaçaoResponse
            {
                Token = tokenString,
                DataExpiracao = tokenDescriptor.Expires
            };
    }
}