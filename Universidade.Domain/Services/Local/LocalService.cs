using Universidade.Domain.Interfaces.Service;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Contracts.Http;
using Microsoft.Extensions.Options;
using Universidade.Domain.Utils;
using Newtonsoft.Json;

namespace Universidade.Domain.Service.Local;
public class LocalService : ILocalService
{
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly HttpCepOptions _httpCepOptions;
    private readonly HttpMunicipioOptions _httpMunicipioOptions;
    private readonly HttpEstadoOptions _httpEstadoOptions;

    public LocalService(IHttpClientFactory httpClientFactory, IOptions<HttpCepOptions> httpCepOptions,
                        IOptions<HttpMunicipioOptions> httpMunicipioOptions, IOptions<HttpEstadoOptions> httpEstadoOptions)
    {
        _httpClientFactory = httpClientFactory;
        _httpCepOptions = httpCepOptions.Value;
        _httpMunicipioOptions = httpMunicipioOptions.Value;
        _httpEstadoOptions = httpEstadoOptions.Value;
    }

    public async Task<LocalResponse> ObterDadosPorCep(string cep)
    {
        LocalValidator.ValidaCep(cep);

        var _cepSomenteNumeros = Utils.Utils.ApenasNumeros(cep);

        var _httpCliente = _httpClientFactory.CreateClient();
        var response = await _httpCliente.GetAsync($"{_httpCepOptions.Url}/{_cepSomenteNumeros}/{_httpCepOptions.AposQueryString}/");

        if (!response.IsSuccessStatusCode)
        {
            throw new Exception("Informações não encontradas");
        }

        var content = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<LocalResponse>(content);
    }

    public async Task<object> ObterDadosPorMunicipio()
    {

        var _httpCliente = _httpClientFactory.CreateClient();
        var response = await _httpCliente.GetAsync($"{_httpMunicipioOptions.Url}");

        if (!response.IsSuccessStatusCode)
        {
            throw new Exception("Informações não encontradas");
        }

        var content = await response.Content.ReadAsStringAsync();
        return content;
    }

    public async Task<object> ObterDadosPorEstado()
    {
        var _httpCliente = _httpClientFactory.CreateClient();
        var response = await _httpCliente.GetAsync($"{_httpEstadoOptions.Url}");

        if (!response.IsSuccessStatusCode)
        {
            throw new Exception("Informações não encontradas");
        }

        var content = await response.Content.ReadAsStringAsync();
        return content;
    }
}
