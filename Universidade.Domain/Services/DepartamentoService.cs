using System.Linq.Expressions;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;

namespace Universidade.Domain.Service;

public class DepartamentoService: BaseService<DepartamentoEntity>, IDepartamentoService
{
    private readonly IDepartamentoRepository _repository;

    
    public DepartamentoService(IDepartamentoRepository departamentoRepository, IHttpContextAccessor httpContextAccessor) : base(departamentoRepository, httpContextAccessor)
    {
        _repository = departamentoRepository;    
    }

    public async Task AtualizarNomeAsync(int id, string nome)
    {
        var entity = await ObterPorIdAsync(id);
        entity.Nome = nome;
        entity.DataAlteracao = DateTime.Now;
        entity.UsuarioAlteracao = UserId;
        await _repository.EditAsync(entity);
    }


    public async Task<DepartamentoEntity> ObterDepartamentoPorCodigoAsync(string codigo)
    {
        var entity = await _repository.FindAsync(x => x.Codigo == codigo && x.Ativo);
        return entity;
    }
}