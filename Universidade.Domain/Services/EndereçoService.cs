using System.Linq.Expressions;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;

namespace Universidade.Domain.Service;

public class EndereçoService: BaseService<EndereçoEntity>, IEndereçoService
{
    private readonly IEndereçoRepository _repository;

    
    public EndereçoService(IEndereçoRepository endereçoRepository, IHttpContextAccessor httpContextAccessor) : base(endereçoRepository, httpContextAccessor)
    {
        _repository = endereçoRepository;    
    }


    public async Task AtualizarComplementoAsync(int id, string complemento)
    {
        var entity = await ObterPorIdAsync(id);
        entity.Complemento = complemento;
        entity.DataAlteracao = DateTime.Now;
        entity.UsuarioAlteracao = UserId;
        await _repository.EditAsync(entity);
    }

    public async Task<EndereçoEntity> ObterEndereçoPorCepAsync(string cep)
    {
        var entity = await _repository.FindAsync(x => x.Cep == cep && x.Ativo);
        return entity;
    }
}