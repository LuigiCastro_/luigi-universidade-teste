using System.Linq.Expressions;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;

namespace Universidade.Domain.Service;

public class PerfilService: BaseService<PerfilEntity>, IPerfilService
{
    private readonly IPerfilRepository _repository;

    
    public PerfilService(IPerfilRepository perfilRepository, IHttpContextAccessor httpContextAccessor) : base(perfilRepository, httpContextAccessor)
    {
        _repository = perfilRepository;    
    }
   

    public async Task AtualizarPerfilUsuarioAsync(int id, string perfilUsuario)
    {
        var entity = await ObterPorIdAsync(id);
        entity.Nome = perfilUsuario;
        entity.DataAlteracao = DateTime.Now;
        entity.UsuarioAlteracao = UserId;
        await _repository.EditAsync(entity);
    }
}