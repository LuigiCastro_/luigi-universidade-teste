using System.Linq.Expressions;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;

namespace Universidade.Domain.Service;

public class CursoService: BaseService<CursoEntity>, ICursoService
{
    private readonly ICursoRepository _repository;

    
    public CursoService(ICursoRepository cursoRepository, IHttpContextAccessor httpContextAccessor) : base(cursoRepository, httpContextAccessor)
    {
        _repository = cursoRepository;    
    }
    

    public async Task AtualizarTurnoAsync(int id, string turno)
    {
        var entity = await ObterPorIdAsync(id);
        entity.Turno = turno;
        entity.DataAlteracao = DateTime.Now;
        entity.UsuarioAlteracao = UserId;
        await _repository.EditAsync(entity);
    }

    public async Task<CursoEntity> ObterCursoPorCodigoAsync(string codigo)
    {
        var entity = await _repository.FindAsync(x => x.Codigo == codigo && x.Ativo);
        return entity;
    }
}