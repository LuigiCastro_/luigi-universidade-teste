// using System.Linq.Expressions;
// using Universidade.Domain.Entities;
// using Universidade.Domain.Interfaces.Services;
// using Universidade.Domain.Interfaces.Repositories;
// using Microsoft.AspNetCore.Http;

// namespace Universidade.Domain.Service;

// public class AlunoService: BaseService<AlunoEntity>, IAlunoService
// {
//     private readonly IAlunoRepository _repository;

    
//     public AlunoService(IAlunoRepository alunoRepository, IHttpContextAccessor httpContextAccessor) : base(alunoRepository, httpContextAccessor)
//     {
//         _repository = alunoRepository;    
//     }



//     public async Task AtualizarMatriculaAsync(int id, string matricula)
//     {
//         var entity = await ObterPorIdAsync(id);
//         entity.Matricula = matricula;
//         entity.DataAlteracao = DateTime.Now;
//         entity.UsuarioAlteracao = UserId;
//         await _repository.EditAsync(entity);
//     }


//     public async Task<AlunoEntity> ObterAlunoPorMatriculaAsync(string matricula)
//     {
//         var entity = await _repository.FindAsync(x => x.Matricula == matricula && x.Ativo);
//         return entity;
//     }


// }