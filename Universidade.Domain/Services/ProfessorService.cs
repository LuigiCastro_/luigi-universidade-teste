// using System.Linq.Expressions;
// using Universidade.Domain.Entities;
// using Universidade.Domain.Interfaces.Services;
// using Universidade.Domain.Interfaces.Repositories;
// using Microsoft.AspNetCore.Http;

// namespace Universidade.Domain.Service;

// public class ProfessorService: BaseService<ProfessorEntity>, IProfessorService
// {
//     private readonly IProfessorRepository _repository;

    
//     public ProfessorService(IProfessorRepository professorRepository, IHttpContextAccessor httpContextAccessor) : base(professorRepository, httpContextAccessor)
//     {
//         _repository = professorRepository;    
//     }

//     public async Task AtualizarNomeAsync(int id, string nome)
//     {
//         var entity = await ObterPorIdAsync(id);
//         entity.Nome = nome;
//         entity.DataAlteracao = DateTime.Now;
//         entity.UsuarioAlteracao = UserId;
//         await _repository.EditAsync(entity);
//     }

//     public async Task<ProfessorEntity> ObterProfessorPorCpfAsync(string cpf)
//     {
//         var entity = await _repository.FindAsync(x => x.Cpf == cpf && x.Ativo);
//         return entity;
//     }
// }