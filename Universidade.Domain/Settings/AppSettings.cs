namespace Universidade.Domain.Settings;

public class AppSettings
{
    public string SqlServerConnection {get; set;}
    public string JwtSecurityKey {get; set;}
}