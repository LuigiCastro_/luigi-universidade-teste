namespace Universidade.Domain.Contracts.Http;

public class HttpOptions
{
    public string Url { get; set; }
    public string AposQueryString { get; set; }
}