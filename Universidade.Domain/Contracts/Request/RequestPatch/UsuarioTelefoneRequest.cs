namespace Universidade.Domain.Contracts.Requests;

public class UsuarioTelefoneRequest
{
    public string Telefone { get; set; }   
}