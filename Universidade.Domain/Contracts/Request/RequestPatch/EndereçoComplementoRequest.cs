namespace Universidade.Domain.Contracts.Requests;

public class EndereçoComplementoRequest
{
    public string Complemento { get; set; }
}
