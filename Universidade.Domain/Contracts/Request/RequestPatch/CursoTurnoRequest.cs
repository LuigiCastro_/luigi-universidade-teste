namespace Universidade.Domain.Contracts.Requests;

public class CursoTurnoRequest
{
    public string Turno { get; set; }
}