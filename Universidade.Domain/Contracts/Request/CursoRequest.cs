namespace Universidade.Domain.Contracts.Requests;

public class CursoRequest
{
    public string Nome {get; set;}
    public string Turno {get; set;}
    public string Duraçao {get; set;}
    public string TipoCurso {get; set;}
    public string Codigo {get; set;}

    public int DepartamentoId {get; set;}

}