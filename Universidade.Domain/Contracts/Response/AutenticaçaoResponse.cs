namespace Universidade.Domain.Contracts.Response;

public class AutenticaçaoResponse
{
    public string Token { get; set; }
    public DateTime? DataExpiracao { get; set; }

}