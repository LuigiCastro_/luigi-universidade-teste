using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Entities;

namespace Universidade.Domain.Contracts.Response;

public class UsuarioResponse : BaseResponse
{
    public string Nome { get; set; }
    public string Cpf {get; set;}
    public string Matricula {get; set;}
    public string Email { get; set; }
    public string Senha { get; set; }
    public string Telefone { get; set; }
    public DateTime? DataNascimento {get; set;}
    public DateTime? DataInicio {get; set;}
    public int EndereçoId {get; set;}
    public int DepartamentoId {get; set;}
    public int CursoId {get; set;}
    public int PerfilId {get; set;}
    public virtual PerfilResponse Perfil { get; set; }

}