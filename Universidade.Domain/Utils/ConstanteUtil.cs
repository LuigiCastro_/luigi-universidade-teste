namespace Universidade.Domain.Utils;

public static class ConstanteUtil
{
    public const string PerfilAlunoNome = "Aluno";
    public const string PerfilProfessorNome = "Professor";
    public const string PerfilLogadoNome = $"{PerfilAlunoNome}, {PerfilProfessorNome}";
    
}