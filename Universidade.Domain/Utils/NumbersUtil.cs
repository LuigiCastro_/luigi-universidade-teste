namespace Universidade.Domain.Utils
{
    public static class Utils
    {
        public static string ApenasNumeros(string valor)
        {
            var somenteNumeros = string.Empty;

            foreach (var _string in valor)
            {
                if (char.IsDigit(_string))
                {
                    somenteNumeros += _string;
                }
            }

            return somenteNumeros;
        }
    }
}
