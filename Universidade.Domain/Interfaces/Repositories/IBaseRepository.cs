using System.Linq.Expressions;

namespace Universidade.Domain.Interfaces.Repositories;

public interface IBaseRepository<T> where T : class
{
    Task<T> FindAsync(int id);
    Task<T> FindAsync(Expression<Func<T, bool>> expression);
    Task<T> FindAsNoTrackingAsync(Expression<Func<T, bool>> expression);
    Task<List<T>> ListAsync(Expression<Func<T, bool>> expression);
    Task AddAsync(T item);
    Task RemoveAsync(T item);
    Task EditAsync(T item);
}