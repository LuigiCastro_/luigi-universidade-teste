using Universidade.Domain.Entities;

namespace Universidade.Domain.Interfaces.Services;

public interface IDepartamentoService : IBaseService<DepartamentoEntity>
{
    Task AtualizarNomeAsync(int id, string nome);
    Task<DepartamentoEntity> ObterDepartamentoPorCodigoAsync(string codigo);
}