using Universidade.Domain.Contracts.Response;

namespace Universidade.Domain.Interfaces.Service
{
    public interface ILocalService
    {
        Task<LocalResponse> ObterDadosPorCep(string cep);
        Task<object> ObterDadosPorMunicipio();
        Task<object> ObterDadosPorEstado();
    }
}
