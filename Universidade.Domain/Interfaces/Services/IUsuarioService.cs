using System.Linq.Expressions;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Entities;

namespace Universidade.Domain.Interfaces.Services;

public interface IUsuarioService : IBaseService<UsuarioEntity>
{
    Task CriarUsuarioAsync(UsuarioEntity usuarioEntity);
    Task AtualizarUsuarioAsync(UsuarioEntity usuarioEntity);
    Task<AutenticaçaoResponse> AutenticarUsuarioAsync(string email, string senha);
    Task AtualizarTelefoneAsync(int id, string telefone);
    Task<List<UsuarioEntity>> ObterTodosUsuarioAsync();
    Task<UsuarioEntity> ObterUsuarioPorIdAsync(int id);
    
}