using Universidade.Domain.Entities;

namespace Universidade.Domain.Interfaces.Services;

public interface IEndereçoService : IBaseService<EndereçoEntity>
{
    Task AtualizarComplementoAsync(int id, string complemento);
    Task<EndereçoEntity> ObterEndereçoPorCepAsync(string cep);
}