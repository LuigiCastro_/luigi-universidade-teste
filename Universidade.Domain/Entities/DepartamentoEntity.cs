namespace Universidade.Domain.Entities;

public class DepartamentoEntity : BaseEntity
{
    public string Nome {get; set;}
    public string Codigo {get; set;}
    public int EndereçoId {get; set;}
    public virtual EndereçoEntity Endereço {get; set;}

    public virtual ICollection<UsuarioEntity> Usuarios {get; set;}
    public virtual ICollection<CursoEntity> Cursos {get; set;}
    // public virtual ICollection<ProfessorEntity> Professores {get; set;}
}