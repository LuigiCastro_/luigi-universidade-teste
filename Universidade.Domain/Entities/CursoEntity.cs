namespace Universidade.Domain.Entities;

public class CursoEntity : BaseEntity
{
    public string Nome {get; set;}
    public string Turno {get; set;}
    public string Duraçao {get; set;}
    public string TipoCurso {get; set;}
    public string Codigo {get; set;}
    
    public int DepartamentoId {get; set;}
    public virtual DepartamentoEntity Departamento {get; set;}

    public virtual ICollection<UsuarioEntity> Usuarios {get; set;}
    // public virtual ICollection<AlunoEntity> Alunos {get; set;}
}