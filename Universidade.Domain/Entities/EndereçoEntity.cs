namespace Universidade.Domain.Entities;

public class EndereçoEntity : BaseEntity
{
    public string Rua {get; set;}
    public string Bairro {get; set;}
    public string Complemento {get; set;}
    public string Cidade {get; set;}
    public string Estado {get; set;}
    public string Cep {get; set;}

    public virtual ICollection<UsuarioEntity> Usuarios {get; set;}
    // public virtual ICollection<AlunoEntity> Alunos {get; set;}
    // public virtual ICollection<ProfessorEntity> Professores {get; set;}
    public virtual ICollection<DepartamentoEntity> Departamentos {get; set;}

}