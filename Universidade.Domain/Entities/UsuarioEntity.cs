namespace Universidade.Domain.Entities;

public class UsuarioEntity : BaseEntity
{
    public string Nome { get; set; }
    public string Cpf {get; set;}
    public string Matricula {get; set;}
    public string Email { get; set; }
    public string Senha { get; set; }
    public string Telefone { get; set; }
    public DateTime? DataNascimento {get; set;}
    public DateTime? DataInicio {get; set;}
    public int EndereçoId {get; set;}
    public virtual EndereçoEntity Endereço {get; set;}
    public int DepartamentoId {get; set;}
    public virtual DepartamentoEntity Departamento {get; set;}
    public int CursoId {get; set;}
    public virtual CursoEntity Curso {get; set;}

    public int PerfilId {get; set;}
    public virtual PerfilEntity Perfil { get; set; }


}